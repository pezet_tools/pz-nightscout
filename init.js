db = connect('mongodb://<HOST>/nightscout_db');
if (db.getUser("<DB_USERNAME>") == null) {
    db.createUser({user: "<DB_USERNAME>", pwd: "<DB_PASS>", roles: [{ role: "readWrite", db: "nightscout_db" }]}); 
}