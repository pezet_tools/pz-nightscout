# pz-nightscout

Zestaw skryptów do automatycznego postawienia aplikacji Nightscout specjalnie na **raspberry pi**.

### Baza danych MongoDB <img src="https://webimages.mongodb.com/_com_assets/cms/kuyjf3vea2hg34taa-horizontal_default_slate_blue.svg?auto=format%252Ccompress" alt="drawing" width="200"/>
Baza danych aplikacji, zawierająca wszystkie pomiary
- oficjalna strona https://www.mongodb.com/

### Aplikacja Nightscout <img src="https://images.squarespace-cdn.com/content/v1/55f63267e4b0b1dfc5acf418/1442203069181-92NIEDXNKCFH2N60T7VS/ns+foundation+green+text.png?format=375w" alt="drawing" width="200"/>
Aplikacja Nightscout wraz interfejsem graficznym
  - oficjalna strona http://nightscout.pl/ 
  - oficjalna strona polska http://nightscout.pl/
  - oficjalna dokumentacja https://nightscout.github.io/

### Automatyczny Uploader danych z LibreLinkUp (rozwiązanie autorskie)
Automat aktualizujący aplikację Nightscout o dane pochodzące z LibreLinkUp
  - strona z kodem źródłowym aplikacji - https://github.com/timoschlueter/nightscout-librelink-up

### Wykorzystane obrazy dockerowe <img src="https://www.gravatar.com/avatar/7510e100f7ebeca4a0b8c3c617349295?s=80&r=g&d=mm" alt="drawing" width="60"/>
- MongoDB - https://hub.docker.com/r/arm64v8/mongo
- Nightscout - https://hub.docker.com/r/dhermanns/rpi-nightscout
- Uploader LibreLinkUp - https://hub.docker.com/r/timoschlueter/nightscout-librelink-up

## Przygotowanie

Przed pierwszym uruchomieniem należy przeprowadzić kilka czynności konfiguracyjnych, które umożliwią dalsze korzystanie z aplikacji.

Należy zatem:
- zainstalować aplikacje docker oraz docker compose
- sklonować nieniejsze repozytorium do wybranej lokalizacji
- w przypadku chęci automatycznego aktualizowania aplikacji o pomiary z LibreLinkUp, należy utworzyć konto w LibreLinkUp i połączyć je ze swoim kontem Libre
```bash
mkdir -p <wybrana_lokalizacja> && cd <wybrana_lokalizacja>
git clone https://gitlab.com/pezet_tools/pz-nightscout.git . && rm -rf .git
```
- utworzyć odpowiednie foldery
```bash
mkdir -p ./volumes/db/data
```
- w plikach .env oraz init.js uzupełnić wszystkie zmienne
```dotenv
DB_USERNAME="<DB_USERNAME>"
DB_PASS="<DB_PASS>"
DB_DATABASE="<DB_DATABASE>"
DB_PORT="<DB_PORT>"
APP_PORT="<APP_PORT>"
HOST="<HOST>"
API_SECRET="<API_SECRET>"
API_SECRET_SHA="<API_SECRET_SHA>"
LINK_UP_USERNAME="<LINK_UP_USERNAME>"
LINK_UP_PASSWORD="<LINK_UP_PASSWORD>"
```
Zmienną API_SECRET_SHA można wyliczyć poniższym poleceniem
```bash
echo -n "<API_SECRET>" | sha1sum | cut -d ' ' -f 1
```

## Uruchomienie
W celu uruchomienia aplikacji należy wykonać poniższe polecenie
```bash
docker compose -f docker-compose.yml up -d
```
Po uruchomieniu aplikacji należy przejść na adres jej strony (na podstawie konfiguracji HOST:APP_PORT) i zatwierdzić podstawowe parametry
```html
http://192.168.1.2:1337
```

**Po tych czynnościach, aplikacja jest gotowa do pracy**